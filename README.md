# Servicio de configuración de cliente de la prueba técnica de SOSAFE

> Walter Devia

**[Consultar API REST del servicio](https://client-setup-service.walterdl.now.sh/api)**

## Advertencia de rendimiento

El despliegue se encuentra en un entorno gratuito y por tanto limitado. Pueden haber retrasos en las respuestas o peticiones http perdidas.

## Propósito

Provee mediante API REST las claves de las APIs de [Pusher Channel](https://pusher.com/channels) y Google Maps.

## Ejecución local

Ejecutar `npm run start-dev` tras instalar dependencias. Notar que se requieren variables de entorno con claves de API [Pusher Channel](https://pusher.com/channels) y Google Maps. Puede crear un archivo `.env` basado del archivo `.env.example` para iniciar.

## Tecnologías

- NodeJS
- TypeScript 3+

## Estándar de codificación

El proyecto usa `ESLint` junto con las extensiones para TypeScript. Antes de cada commit el análisis de cumplimiento del estandar se ejecuta automáticamente para asegurar que no se incluya en el repositorio código que infrinja el estandar. Esto se hace mediante la librería [husky](https://www.npmjs.com/package/husky`). Ver las reglas del estandar en el archivo `.eslintrc.json`.

## Despliegue

Desplegado en [Zeit Now](https://zeit.co/) como [servicio serverless](https://zeit.co/docs/v2/serverless-functions/introduction/) con despliegue contínuo habilitado integrado con GitLab. Los últimos cambios se ven reflejados en la url del servicio tras fusionar commits a la rama master del repositorio.

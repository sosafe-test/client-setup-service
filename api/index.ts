import { NowRequest, NowResponse } from '@now/node'

export default function(request: NowRequest, response: NowResponse) {
  setCors(response)

  if (isRequestMethod(request, 'options')) {
    return sendCors(response)
  }

  return response.json({
    googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY,
    pusherKey: process.env.PUSHER_KEY
  })
}

function isRequestMethod(request: NowRequest, method: string) {
  return typeof request.method === 'string' && request.method.toLowerCase() === method
}

function setCors(response: NowResponse) {
  response.setHeader('Access-Control-Allow-Origin', '*')
  response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST')
  response.setHeader('Access-Control-Allow-Headers', '*')
}

function sendCors(response: NowResponse) {
  response.writeHead(200)
  response.end()
}

